<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RetiredArtifact Entity
 *
 * @property int $id
 * @property string|null $artifact_id
 * @property string|null $new_artifact_id
 * @property string|null $artifact_remarks
 * @property bool|null $is_public
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\NewArtifact $new_artifact
 */
class RetiredArtifact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'new_artifact_id' => true,
        'artifact_remarks' => true,
        'is_public' => true,
        'artifact' => true,
        'new_artifact' => true
    ];
}
