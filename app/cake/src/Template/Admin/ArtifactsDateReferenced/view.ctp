<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDateReferenced $artifactsDateReferenced
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Artifacts Date Referenced'), ['action' => 'edit', $artifactsDateReferenced->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Artifacts Date Referenced'), ['action' => 'delete', $artifactsDateReferenced->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDateReferenced->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts Date Referenced'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifacts Date Referenced'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="artifactsDateReferenced view large-9 medium-8 columns content">
    <h3><?= h($artifactsDateReferenced->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Artifact') ?></th>
            <td><?= $artifactsDateReferenced->has('artifact') ? $this->Html->link($artifactsDateReferenced->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsDateReferenced->artifact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ruler') ?></th>
            <td><?= $artifactsDateReferenced->has('ruler') ? $this->Html->link($artifactsDateReferenced->ruler->id, ['controller' => 'Rulers', 'action' => 'view', $artifactsDateReferenced->ruler->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month') ?></th>
            <td><?= $artifactsDateReferenced->has('month') ? $this->Html->link($artifactsDateReferenced->month->id, ['controller' => 'Months', 'action' => 'view', $artifactsDateReferenced->month->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month No') ?></th>
            <td><?= h($artifactsDateReferenced->month_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Day No') ?></th>
            <td><?= h($artifactsDateReferenced->day_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($artifactsDateReferenced->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Year Id') ?></th>
            <td><?= $this->Number->format($artifactsDateReferenced->year_id) ?></td>
        </tr>
    </table>
</div>
