<div>
	<?= __('Publication Data')?>
	<?= $this->Form->create("",['type'=>'get']) ?>
	<?= $this->Form->control('Primary_Publication'); ?>
    <?= $this->Form->control('Secondary_Publication'); ?>
	<?= $this->Form->control('Author(s)'); ?>
	<?= $this->Form->control('Date_of_Publication'); ?>
	<?= $this->Form->control('CDLI_no'); ?>
    <?= __('Object Data')?>
	<?= $this->Form->control('Object_Type'); ?>
	<?= $this->Form->control('Material'); ?>
    <?= $this->Form->control('Collection'); ?>
    <?= $this->Form->control('Provenience'); ?>
    <?= $this->Form->control('Period'); ?>
    <?= $this->Form->control('Object_Remarks'); ?>
    <?= __('Publication Data')?>
	<?= $this->Form->control('Comment'); ?>
	<?= $this->Form->control('Structure'); ?>
    <?= $this->Form->control('Transliteration'); ?>
	
    <?= $this->Form->control('Translation'); ?>
    <?= $this->Form->control('Language'); ?>
    <?= $this->Form->control('Genre'); ?>
   
    <?= $this->Form->control('Credits'); ?>
    <?= $this->Form->control('ATF_Source'); ?>
    <?= $this->Form->control('Catalogue_Source'); ?>
    <?= $this->Form->control('Translation_Source'); ?>

	<?= $this->Form->control('Artifacts_Composites'); ?>
	<?= $this->Form->control('Seal_Number'); ?>
	<?= $this->Form->control('Musuem_Number'); ?>
	<?= $this->Form->control('Excavation_Number'); ?>
	<?= $this->Form->control('Accession_Number'); ?>
	<?= $this->Form->control('Publication_Number'); ?>
	<?= $this->Form->button('Search', ['type' => 'submit']); ?>
	<?= $this->Form->end()?>
</div>

<?php if(!empty($result)){?>
	<?php foreach ($result as $row) {?>

		<table>
			<tr>
				<th rowspan="10"> 
					<?php echo $row["id"];?>
				</th>
				<th > 
					<?php echo $row["_matchingData"]["Publications"]["designation"];?>
				</th>
			</tr>
			<tr>
				<td >
					<?php echo "Author: ";?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Publications Date: ".$row["_matchingData"]["Publications"]["year"]; ?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "CDLI No.: ".$row["id"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Collection: ".$row["_matchingData"]["Collections"]["collection"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Period: ".$row["_matchingData"]["Periods"]["period"];?>
				</td>
			</tr>		
			<tr>
				<td >
					<?php echo "Object Type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Material: ".$row["_matchingData"]["Materials"]["material"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Transliteration/Translation: ".$row["_matchingData"]["Inscriptions"]["transliteration"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Languages: ".$row["_matchingData"]["Languages"]["language"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Genres: ".$row["_matchingData"]["Genres"]["genre"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "ATF Source: ".$row["atf_source"];?>
					
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Catalogue Source: ".$row["db_source"];?>
					
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Translation Source: ".$row["translation_source"];?>
					
				</td>
			</tr>
		</table>

	<?php }?>
<?php }?>

<hr class="my-5">

<div class="visualize">
    <div class="pills-container d-flex justify-content-center">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-bar-chart-tab" data-toggle="pill" href="#pills-bar-chart" role="tab" aria-controls="pills-bar-chart" aria-selected="true">Bar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-donut-chart-tab" data-toggle="pill" href="#pills-donut-chart" role="tab" aria-controls="pills-donut-chart" aria-selected="false">Donut Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-radar-chart-tab" data-toggle="pill" href="#pills-radar-chart" role="tab" aria-controls="pills-radar-chart" aria-selected="false">Radar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-line-chart-tab" data-toggle="pill" href="#pills-line-chart" role="tab" aria-controls="pills-line-chart" aria-selected="false">Line Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-dendrogram-chart-tab" data-toggle="pill" href="#pills-dendrogram-chart" role="tab" aria-controls="pills-dendrogram-chart" aria-selected="false">Dendrogram Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-choropleth-map-tab" data-toggle="pill" href="#pills-choropleth-map" role="tab" aria-controls="pills-choropleth-map" aria-selected="false">Choropleth Map</a>
            </li>
        </ul>
    </div>
    
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-bar-chart" role="tabpanel" aria-labelledby="pills-bar-chart-tab">
            <div id="bar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/bar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Bar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
      
        <div class="tab-pane fade" id="pills-donut-chart" role="tabpanel" aria-labelledby="pills-donut-chart-tab">
            <div id="donut-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/donut.png', ['class' => 'fallback-image', 'alt' => 'CDLI Donut Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-radar-chart" role="tabpanel" aria-labelledby="pills-radar-chart-tab">
            <div id="radar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/radar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Radar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-line-chart" role="tabpanel" aria-labelledby="pills-line-chart-tab">
            <div class="d-flex">
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="line-chart-filter">Line Chart </label>
                            <select class="form-control" id="line-chart-filter">
                                <option value="period-vs-genre" selected>Period vs Genre</option>
                                <option value="period-vs-language">Period vs Language</option>
                                <option value="period-vs-material">Period vs Material</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="line-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/line.png', ['class' => 'fallback-image', 'alt' => 'CDLI Line Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-dendrogram-chart" role="tabpanel" aria-labelledby="pills-dendrogram-chart-tab">
            <div id="dendrogram-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/dendrogram.png', ['class' => 'fallback-image', 'alt' => 'CDLI Dendrogram Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-choropleth-map" role="tabpanel" aria-labelledby="pills-choropleth-map-tab">
            <div class="d-flex">
                <div class="map-controls-container">
                    <div class="btn-group" role="group" aria-label="Map Controls">
                        <button type="button" id="zoom-in" class="btn btn-secondary">
                            Zoom In <i class="fa fa-search-plus ml-2" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="zoom-out" class="btn btn-secondary">
                            Zoom Out <i class="fa fa-search-minus ml-2" aria-hidden="true"></i>
                        </button>
                        <button type="button" id="reset" class="btn btn-secondary">
                            Reset
                        </button>
                    </div>
                </div>
                
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="choropleth-map-filter">Filter Map By</label>
                            <select class="form-control" id="choropleth-map-filter">
                                <option value="proveniences" selected>Proveniences</option>
                                <option value="regions">Regions</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="choropleth-map">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/choropleth.png', ['class' => 'fallback-image', 'alt' => 'CDLI Choropleth Map']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
    </div>
</div>

<?php echo $this->Html->script(['d3', 'd3-compiled']); ?>

<script type="text/javascript">
    var barChartData = <?php echo $barChart; ?>;
    var donutChartData = <?php echo $donutChart; ?>;
    var radarChartData = <?php echo $radarChart; ?>;
    var lineChartData = <?php echo $lineChart; ?>;
    var dendrogramChartData = <?php echo $dendrogramChart; ?>;
    var choroplethMapData = <?php echo $choroplethMap; ?>;
    
    barChart(barChartData);
    donutChart(donutChartData);
    radarChart(radarChartData);
    lineChart(lineChartData);
    dendrogramChart(dendrogramChartData);
    choroplethMap(choroplethMapData[0], "proveniences");
    
    
    // Line Chart Select Filter
    $('#line-chart-filter').change(function() {        
        var lineChartType = $(this).val();
        var targetURL = "<?= \Cake\Routing\Router::url(array('controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData'), true) ?>";
        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        
        // Create AJAX request for retrieving updated data
        $.ajax({
            type: "POST",
            url: targetURL,
            headers: { 'X-CSRF-Token': csrfToken },
            data: {lineChartType: lineChartType},
            dataType: "JSON",
            success: function(response) {
                // Remove the current SVG
                $("#line-chart").empty();
                
                // Add the new line chart
                lineChart(response);
            }
        })
    })
    
    
    // Get choropleth map by filter
    // 0 : Proveniences
    // 1 : Regions
    $('#choropleth-map-filter').change(function() {        
        // Remove the current SVG
        $("#choropleth-map").empty();
        
        var choroplethMapType = $(this).val();
        if (choroplethMapType == "proveniences")
            choroplethMap(choroplethMapData[0], choroplethMapType);
        else
            choroplethMap(choroplethMapData[1], choroplethMapType);
    })
</script>