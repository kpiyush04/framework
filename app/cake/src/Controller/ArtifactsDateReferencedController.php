<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsDateReferenced Controller
 *
 * @property \App\Model\Table\ArtifactsDateReferencedTable $ArtifactsDateReferenced
 *
 * @method \App\Model\Entity\ArtifactsDateReferenced[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsDateReferencedController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Rulers', 'Months', 'Years']
        ];
        $artifactsDateReferenced = $this->paginate($this->ArtifactsDateReferenced);

        $this->set(compact('artifactsDateReferenced'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Date Referenced id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsDateReferenced = $this->ArtifactsDateReferenced->get($id, [
            'contain' => ['Artifacts', 'Rulers', 'Months', 'Years']
        ]);

        $this->set('artifactsDateReferenced', $artifactsDateReferenced);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsDateReferenced = $this->ArtifactsDateReferenced->newEntity();
        if ($this->request->is('post')) {
            $artifactsDateReferenced = $this->ArtifactsDateReferenced->patchEntity($artifactsDateReferenced, $this->request->getData());
            if ($this->ArtifactsDateReferenced->save($artifactsDateReferenced)) {
                $this->Flash->success(__('The artifacts date referenced has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts date referenced could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsDateReferenced->Artifacts->find('list', ['limit' => 200]);
        $rulers = $this->ArtifactsDateReferenced->Rulers->find('list', ['limit' => 200]);
        $months = $this->ArtifactsDateReferenced->Months->find('list', ['limit' => 200]);
        $years = $this->ArtifactsDateReferenced->Years->find('list', ['limit' => 200]);
        $this->set(compact('artifactsDateReferenced', 'artifacts', 'rulers', 'months', 'years'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Date Referenced id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsDateReferenced = $this->ArtifactsDateReferenced->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsDateReferenced = $this->ArtifactsDateReferenced->patchEntity($artifactsDateReferenced, $this->request->getData());
            if ($this->ArtifactsDateReferenced->save($artifactsDateReferenced)) {
                $this->Flash->success(__('The artifacts date referenced has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts date referenced could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsDateReferenced->Artifacts->find('list', ['limit' => 200]);
        $rulers = $this->ArtifactsDateReferenced->Rulers->find('list', ['limit' => 200]);
        $months = $this->ArtifactsDateReferenced->Months->find('list', ['limit' => 200]);
        $years = $this->ArtifactsDateReferenced->Years->find('list', ['limit' => 200]);
        $this->set(compact('artifactsDateReferenced', 'artifacts', 'rulers', 'months', 'years'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Date Referenced id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsDateReferenced = $this->ArtifactsDateReferenced->get($id);
        if ($this->ArtifactsDateReferenced->delete($artifactsDateReferenced)) {
            $this->Flash->success(__('The artifacts date referenced has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts date referenced could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
