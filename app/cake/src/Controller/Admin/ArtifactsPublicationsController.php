<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactsPublications Controller
 *
 * @property \App\Model\Table\ArtifactsPublicationsTable $ArtifactsPublications
 *
 * @method \App\Model\Entity\ArtifactsPublication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsPublicationsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Publications']
        ];
        $artifactsPublications = $this->paginate($this->ArtifactsPublications);

        $this->set(compact('artifactsPublications'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsPublication = $this->ArtifactsPublications->get($id, [
            'contain' => ['Artifacts', 'Publications']
        ]);

        $this->set('artifactsPublication', $artifactsPublication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsPublication = $this->ArtifactsPublications->newEntity();
        if ($this->request->is('post')) {
            $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
            if ($this->ArtifactsPublications->save($artifactsPublication)) {
                $this->Flash->success(__('The artifacts publication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts publication could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsPublications->Artifacts->find('list', ['limit' => 200]);
        $publications = $this->ArtifactsPublications->Publications->find('list', ['limit' => 200]);
        $this->set(compact('artifactsPublication', 'artifacts', 'publications'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsPublication = $this->ArtifactsPublications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsPublication = $this->ArtifactsPublications->patchEntity($artifactsPublication, $this->request->getData());
            if ($this->ArtifactsPublications->save($artifactsPublication)) {
                $this->Flash->success(__('The artifacts publication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts publication could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsPublications->Artifacts->find('list', ['limit' => 200]);
        $publications = $this->ArtifactsPublications->Publications->find('list', ['limit' => 200]);
        $this->set(compact('artifactsPublication', 'artifacts', 'publications'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsPublication = $this->ArtifactsPublications->get($id);
        if ($this->ArtifactsPublications->delete($artifactsPublication)) {
            $this->Flash->success(__('The artifacts publication has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts publication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
